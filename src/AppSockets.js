import React, { Component } from 'react';
import './App.css';

window.chatSocket = new WebSocket('ws://localhost:3001/cable');

class AppSockets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentChatMessage: '',
      chatLogs: []
    };
  }

  updateCurrentChatMessage(event) {
    this.setState({
      currentChatMessage: event.target.value
    });
  }

  createSocket() {
    window.chatSocket.onopen = (event) => {
      const subscribeMsg = {"command":"subscribe","identifier":"{\"channel\":\"ChatChannel\"}"}
      window.chatSocket.send(JSON.stringify(subscribeMsg))
    }

    window.chatSocket.onmessage = event => {
      const result = JSON.parse(event.data);
      let chatLogs = this.state.chatLogs;

      if (result.message?.content) {
        chatLogs.push(result.message);
        this.setState({ chatLogs: chatLogs });
      }
    };
  }

  renderChatLog() {
    return this.state.chatLogs.map((el) => {
      return (
        <li key={`chat_${el.id}`}>
          <span className='chat-message'>{ el.content }</span>
          <span className='chat-created-at'>{ el.created_at }</span>
        </li>
      );
    });
  }

  handleSendEvent(event) {
    event.preventDefault();
    
    const msg = {
      "command":"message",
      "identifier":'{"channel":"ChatChannel"}',
      "data":`{
        "action": "create",
        "content": "${this.state.currentChatMessage}"
      }`
    }

    window.chatSocket.send(JSON.stringify(msg))

    this.setState({
      currentChatMessage: ''
    });
  }

  handleChatInputKeyPress(event) {
    if(event.key === 'Enter') {
      this.handleSendEvent(event);
    }
  }

  componentWillMount() {
    this.createSocket();
  }

  render() {
    return (
      <div className='App'>
        <div className='stage'>
          <h1>Chat</h1>

          <ul className='chat-logs'>
            { this.renderChatLog() }
          </ul>

          <input
            onKeyPress={ (e) => this.handleChatInputKeyPress(e) }
            value={ this.state.currentChatMessage }
            onChange={ (e) => this.updateCurrentChatMessage(e) }
            type='text'
            placeholder='Enter your message...'
            className='chat-input' />

          <button
            onClick={ (e) => this.handleSendEvent(e) }
            className='send'>
            Send
          </button>
        </div>
      </div>
    );
  }
}

export default AppSockets;
